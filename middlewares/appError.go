package middlewares

import (
	"log"
	"net/http"
)

type AppHandler func(http.ResponseWriter, *http.Request) error

func (fn AppHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if e := fn(w, r); e != nil { // e is *appError, not os.Error.
		log.Printf("%v", e.Error())
		http.Error(w, e.Error(), 500)
	}
}
