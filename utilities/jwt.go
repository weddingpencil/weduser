package utilities

// Example HTTP auth using asymmetric crypto/RSA keys
// This is based on a (now outdated) example at https://gist.github.com/cryptix/45c33ecf0ae54828e63b

import (
	"bytes"
	"crypto/rsa"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/dgrijalva/jwt-go/request"

	"gitlab.com/weddingpencil/weduser/models"
)

// location of the files used for signing and verification
const (
	privKeyPath = "/Users/sydmacair/.ssh/wed-uat-200607"           // openssl genrsa -out app.rsa keysize
	pubKeyPath  = "/Users/sydmacair/.ssh/wed-uat-200607-pkcs8.pub" // openssl rsa -in app.rsa -pubout > app.rsa.pub
)

var (
	verifyKey  *rsa.PublicKey
	signKey    *rsa.PrivateKey
	serverPort int
)

// read the key files before starting http handlers
func InitJwt() {
	signBytes, err := ioutil.ReadFile(privKeyPath)
	fatal(err)

	signKey, err = jwt.ParseRSAPrivateKeyFromPEM(signBytes)
	fatal(err)

	verifyBytes, err := ioutil.ReadFile(pubKeyPath)
	fatal(err)

	verifyKey, err = jwt.ParseRSAPublicKeyFromPEM(verifyBytes)
	fatal(err)

	//http.HandleFunc("/authenticate", authHandler)
	//http.HandleFunc("/restricted", restrictedHandler)

	// Setup listener
	//listener, err := net.ListenTCP("tcp", &net.TCPAddr{})
	//serverPort = listener.Addr().(*net.TCPAddr).Port

	//log.Println("Listening...")

}

var start func()

func fatal(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

// Define some custom types were going to use within our tokens
type UserInfo struct {
	ID   uint
	Name string
	Kind string
}

type CustomClaims struct {
	*jwt.StandardClaims
	TokenType string
	UserInfo
}

func Example_getTokenViaHTTP() {
	// See func authHandler for an example auth handler that produces a token
	res, err := http.PostForm(fmt.Sprintf("http://localhost:%v/authenticate", serverPort), url.Values{
		"user": {"test"},
		"pass": {"known"},
	})
	if err != nil {
		fatal(err)
	}

	if res.StatusCode != 200 {
		fmt.Println("Unexpected status code", res.StatusCode)
	}

	// Read the token out of the response body
	buf := new(bytes.Buffer)
	io.Copy(buf, res.Body)
	res.Body.Close()
	tokenString := strings.TrimSpace(buf.String())

	// Parse the token
	token, err := jwt.ParseWithClaims(tokenString, &CustomClaims{}, func(token *jwt.Token) (interface{}, error) {
		// since we only use the one private key to sign the tokens,
		// we also only use its public counter part to verify
		return verifyKey, nil
	})
	fatal(err)

	claims := token.Claims.(*CustomClaims)
	fmt.Println(claims.UserInfo.Name)

	//Output: test
}

func CreateJwtToken(user *models.User) (string, error) {
	// create a signer for rsa 256
	t := jwt.New(jwt.GetSigningMethod("RS256"))

	// set our claims
	t.Claims = &CustomClaims{
		&jwt.StandardClaims{
			// set the expire time
			// see http://tools.ietf.org/html/draft-ietf-oauth-json-web-token-20#section-4.1.4
			Id:        fmt.Sprint(user.ID),
			ExpiresAt: time.Now().Add(time.Minute * 30).Unix(),
		},
		"level1",
		UserInfo{
			ID:   user.ID,
			Name: fmt.Sprintf("%v %v", user.FirstName, user.LastName),
			Kind: "human",
		},
	}

	// Create token string
	return t.SignedString(signKey)
}

//VerifyJwtToken will compare token from the request
func VerifyJwtToken(r *http.Request) (uint, error) {
	token, err := request.ParseFromRequestWithClaims(r, request.OAuth2Extractor, &CustomClaims{}, func(token *jwt.Token) (interface{}, error) {
		// since we only use the one private key to sign the tokens,
		// we also only use its public counter part to verify
		return verifyKey, nil
	})

	// If the token is missing or invalid, return error
	if err != nil {
		fmt.Printf("Invalid token: %v", err)
		return 0, err
	}
	userID := token.Claims.(*CustomClaims).ID
	// Token is valid
	fmt.Printf("valid token: %v", token.Claims.(*CustomClaims).ID)

	return userID, nil
}

// only accessible with a valid token
func restrictedHandler(w http.ResponseWriter, r *http.Request) {
	// Get token from request
	token, err := request.ParseFromRequestWithClaims(r, request.OAuth2Extractor, &CustomClaims{}, func(token *jwt.Token) (interface{}, error) {
		// since we only use the one private key to sign the tokens,
		// we also only use its public counter part to verify
		return verifyKey, nil
	})

	// If the token is missing or invalid, return error
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		fmt.Fprintln(w, "Invalid token:", err)
		return
	}

	// Token is valid
	fmt.Fprintln(w, "Welcome,", token.Claims.(*CustomClaims).Name)
	return
}
