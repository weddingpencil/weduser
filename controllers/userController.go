package controllers

import (
	"fmt"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"

	"gitlab.com/weddingpencil/weduser/models"
	"gitlab.com/weddingpencil/weduser/repository"
	"gitlab.com/weddingpencil/weduser/utilities"

	ocTrace "go.opencensus.io/trace"
)

func GetUsers(w http.ResponseWriter, r *http.Request) {
	_, span := ocTrace.StartSpan(r.Context(), "SELECT_USERS")
	defer span.End()
	var users []models.User
	models.DB.Find(&users)
	var response = GetUsersResponse{Data: &users}
	render.Render(w, r, &response)
}

func GetUser(w http.ResponseWriter, r *http.Request) {
	var userRef *models.User
	var err error
	userRepo := repository.CreateUserRepository(models.DB)

	if userID := chi.URLParam(r, "userID"); userID != "" {

		userRef, err = userRepo.FindByID(userID)

		if err != nil {
			//render.Render(w, r, &GetUserFailureResponse{})
			w.WriteHeader(500)
			render.Render(w, r, &GetUserFailureResponse{})
			return
			//return err

		}
	}

	render.Render(w, r, &GetUserResponse{Data: userRef})
}

func CreateUser(w http.ResponseWriter, r *http.Request) {
	// Validate input
	data := &CreateUserInput{}
	var err error
	if err = render.Bind(r, data); err != nil {
		panic(err)
	}

	userRepo := repository.CreateUserRepository(models.DB)

	data.Password = utilities.HashPassword([]byte(data.Password))
	user := ConvertCreateUserInputToUser(data)
	err = userRepo.Create(user)
	if err != nil {
		w.WriteHeader(500)
		render.Render(w, r, &GetUserFailureResponse{})
		return
	}
	response := APIResponse{Data: user}
	render.Render(w, r, &response)
}

func Authenticate(w http.ResponseWriter, r *http.Request) {
	// Validate input
	var userRef *models.User
	var err error
	userRepo := repository.CreateUserRepository(models.DB)

	data := &AuthenticateInput{}
	if err := render.Bind(r, data); err != nil {
		panic(err)
	}

	userRef, err = userRepo.FindByEmail(data.Email)
	if err != nil {
		w.WriteHeader(500)
		render.Render(w, r, &GetUserFailureResponse{})
		return
	}

	passwordMatched := utilities.ComparePassword(userRef.Password, []byte(data.Password))
	if !passwordMatched {
		w.WriteHeader(401)
		render.Render(w, r, &GetUserFailureResponse{})
		return
	}

	token, err := utilities.CreateJwtToken(userRef)

	fmt.Println(token, err)

	response := APIResponse{Data: token}
	render.Render(w, r, &response)
}

func PrivateAccess(w http.ResponseWriter, r *http.Request) {
	// Validate input

	userID, err := utilities.VerifyJwtToken(r)

	if err != nil {
		w.WriteHeader(500)
		response := APIResponse{Data: userID}
		render.Render(w, r, &response)
		return
	}

	response := APIResponse{Data: userID}
	render.Render(w, r, &response)
}
