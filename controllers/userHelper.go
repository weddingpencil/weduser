package controllers

import (
	"gitlab.com/weddingpencil/weduser/models"
)

func ConvertCreateUserInputToUser(input *CreateUserInput) *models.User {
	user := &models.User{
		FirstName: input.FirstName,
		LastName:  input.LastName,
		Email:     input.Email,
		Password:  input.Password,
	}
	return user
}
