package controllers

//APIResponse is for successful response
type APIResponse struct {
	Data    interface{} `json:"data"`
	Success bool        `json:"success"`
	Error   Error       `json:"error"`
}
