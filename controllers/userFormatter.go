package controllers

import (
	"errors"
	"net/http"
)

func (input *CreateUserInput) Bind(r *http.Request) error {
	if input.Password == "" {
		return errors.New("password required")
	}

	return nil
}

func (input *AuthenticateInput) Bind(r *http.Request) error {
	if input.Password == "" || input.Email == "" {
		return errors.New("email and password required")
	}

	return nil
}

func (response *GetUsersResponse) Render(w http.ResponseWriter, r *http.Request) error {
	for _, s := range *response.Data {
		s.Password = ""
	}
	response.Success = true
	return nil
}

func (response *GetUserFailureResponse) Render(w http.ResponseWriter, r *http.Request) error {
	response.Success = false
	response.Error.Message = "Cannot find user"
	return nil
}

func (response *GetUserResponse) Render(w http.ResponseWriter, r *http.Request) error {
	response.Data.Password = ""
	response.Success = true
	return nil
}

func (response *GeneralUserResponse) Render(w http.ResponseWriter, r *http.Request) error {
	response.Success = true
	return nil
}

func (response *APIResponse) Render(w http.ResponseWriter, r *http.Request) error {
	response.Success = true
	return nil
}
