package controllers

import (
	"gitlab.com/weddingpencil/weduser/models"
)

type CreateUserInput struct {
	FirstName string `json:"firstName" binding:"required"`
	LastName  string `json:"lastName" binding:"required"`
	Email     string `json:"email" binding:"required"`
	Password  string `json:"password" binding:"required"`
}

type UpdateUserInput struct {
	ID        string `json:"id"`
	FirstName string `json:"title"`
	LastName  string `json:"author"`
	Email     string `json:"table"`
}

type UpdatePasswordInput struct {
	ID       string `json:"id"`
	Password string `json:"password"`
}

type GetUsersResponse struct {
	Data    *[]models.User `json:"data"`
	Success bool           `json:"success"`
	Error   models.Error   `json:"error"`
}

type GetUserFailureResponse struct {
	Data    string       `json:"data"`
	Success bool         `json:"success"`
	Error   models.Error `json:"error"`
}

type GetUserResponse struct {
	Data    *models.User `json:"data"`
	Success bool         `json:"success"`
	Error   models.Error `json:"error"`
}

type GeneralUserResponse struct {
	Data    bool         `json:"data"`
	Success bool         `json:"success"`
	Error   models.Error `json:"error"`
}

type AuthenticateInput struct {
	Email    string `json:"email" binding:"required"`
	Password string `json:"password" binding:"required"`
}
