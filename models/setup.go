package models

import (
	"fmt"
	"os"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

var DB *gorm.DB

func ConnectDatabase() {
	pgHost := os.Getenv("PG_HOST")
	pgPort := os.Getenv("PG_PORT")
	pgUser := os.Getenv("PG_USER")
	pgPassword := os.Getenv("PG_PASSWORD")
	pgDbName := os.Getenv("PG_WED_GUEST_DB")
	fmt.Printf("host=%s port=%s user=%s dbname=%s password=%s sslmode=disable",
		pgHost, pgPort, pgUser, pgDbName, pgPassword)
	db, err := gorm.Open("postgres", fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s sslmode=disable",
		pgHost, pgPort, pgUser, pgDbName, pgPassword))

	if err != nil {
		panic(err)
	}

	db.AutoMigrate(&User{})
	if os.Getenv("ENVIRONMENT") == "dev" {
		db.LogMode(true)
	}
	DB = db
}
