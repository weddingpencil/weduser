module gitlab.com/weddingpencil/weduser

go 1.12

require (
	github.com/Rosaniline/gorm-ut v0.0.0-20190331143837-786b71894576 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.6.3 // indirect
	github.com/go-chi/chi v4.1.1+incompatible
	github.com/go-chi/docgen v1.0.5 // indirect
	github.com/go-chi/jwtauth v4.0.4+incompatible
	github.com/go-chi/render v1.0.1
	github.com/go-test/deep v1.0.6 // indirect
	github.com/jinzhu/gorm v1.9.12
	github.com/stretchr/testify v1.4.0
	go.opencensus.io v0.22.3
	golang.org/x/crypto v0.0.0-20191205180655-e7c4368fe9dd
	gopkg.in/DATA-DOG/go-sqlmock.v1 v1.3.0
)
