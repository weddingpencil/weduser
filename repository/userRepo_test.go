package repository

import (
	"database/sql"
	"regexp"
	"strconv"
	"testing"

	"github.com/jinzhu/gorm"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"

	"gitlab.com/weddingpencil/weduser/models"
)

type Suite struct {
	suite.Suite
	DB   *gorm.DB
	mock sqlmock.Sqlmock

	repository UserRepository
	user       *models.User
}

func (s *Suite) SetupSuite() {
	var (
		db  *sql.DB
		err error
	)

	db, s.mock, err = sqlmock.New()
	require.NoError(s.T(), err)

	s.DB, err = gorm.Open("postgres", db)
	require.NoError(s.T(), err)

	s.DB.LogMode(true)

	s.repository = CreateUserRepository(s.DB)

}

func (s *Suite) AfterTest(_, _ string) {
	require.NoError(s.T(), s.mock.ExpectationsWereMet())
}

func TestInit(t *testing.T) {
	suite.Run(t, new(Suite))
}

func (s *Suite) Test_repository_Find_By_ID() {
	var (
		id        = "1"
		firstName = "test-name"
	)
	uID, err := strconv.ParseUint(id, 10, 64)

	s.mock.ExpectQuery(regexp.QuoteMeta(
		`SELECT * FROM "users" WHERE (id = $1)`)).
		WithArgs(id).
		WillReturnRows(sqlmock.NewRows([]string{"id", "first_name"}).
			AddRow(id, firstName))

	res, err := s.repository.FindByID(id)
	require.NoError(s.T(), err)
	require.Equal(s.T(), &models.User{ID: uint(uID), FirstName: firstName}, res, "Get users are same as input")
}

func (s *Suite) Test_repository_Create() {
	var (
		id        = "1"
		firstName = "Sydney5"
		lastName  = "Wu5"
		email     = "sydney5@test.com"
		password  = "hello"
	)

	uID, err := strconv.ParseUint(id, 10, 64)
	newUser := models.User{
		ID:        uint(uID),
		FirstName: firstName,
		LastName:  lastName,
		Email:     email,
		Password:  password,
	}
	s.mock.ExpectBegin()
	s.mock.ExpectQuery(regexp.QuoteMeta(
		`INSERT INTO "users1" ("id","first_name","last_name","email","password") 
			VALUES ($1,$2,$3,$4,$5) RETURNING "users"."id"`)).
		WithArgs(uint(uID), firstName, lastName, email, password).
		WillReturnRows(
			sqlmock.NewRows([]string{"id"}).
				AddRow(id))

	s.mock.ExpectCommit()

	err = s.repository.Create(&newUser)

	require.NoError(s.T(), err)
}
