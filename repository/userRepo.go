package repository

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/weddingpencil/weduser/models"
)

type UserRepository interface {
	FindByID(userID string) (*models.User, error)
	FindByEmail(email string) (*models.User, error)
	Create(user *models.User) error
}

func (u *userRepo) FindByID(userID string) (*models.User, error) {
	var user models.User

	if err := u.DB.Where("id = ?", userID).First(&user).Error; err != nil {
		return nil, err
	}
	return &user, nil
}

func (u *userRepo) FindByEmail(email string) (*models.User, error) {
	var user models.User

	if err := u.DB.Where("email = ?", email).First(&user).Error; err != nil {
		return nil, err
	}
	return &user, nil
}

func (u *userRepo) Create(user *models.User) error {
	return u.DB.Create(&user).Error

}

type userRepo struct {
	DB *gorm.DB
}

func CreateUserRepository(db *gorm.DB) UserRepository {
	return &userRepo{
		DB: db,
	}
}
